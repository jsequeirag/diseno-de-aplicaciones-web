window.onload = function () {
    cargarArticulos()
}

function cargarArticulos() {

    if(obtenerUsuario()==null){
        document.getElementById('sessionbutton').setAttribute('style','display:none')
        document.getElementById('dashboardbutton').setAttribute('style','display:none')
    }else{
        document.getElementById('registrobutton').setAttribute('style','display:none')
        document.getElementById('loginbutton').setAttribute('style','display:none')
    }


    var articulos = []
    articulos = JSON.parse(localStorage.getItem('Articulos'))

    articulos.sort(((a, b) => a.fecha - b.fecha));

    for (var numero = 0; numero < articulos.length; numero++) {


        console.log('Articulo encontrado')
        var lista = document.getElementById('scroll-container')
        var parentdiv = document.createElement('div')
        var a1 = document.createElement('a')
        var a2 = document.createElement('a')
        var a3 = document.createElement('a')
        var img = document.createElement('img')
        var br1 = document.createElement('br')
        var br2 = document.createElement('br')

        parentdiv.className = "image-container"

        parentdiv.id = articulos[numero].idProducto

        img.className = "img"
        img.src = articulos[numero].url
        img.alt = ""

        img.setAttribute("onclick", "guardar(this.parentNode.id)")
        a1.href = "detalledeproducto.html"
        a1.setAttribute("onclick", "guardar(this.parentNode.id)")
        a1.appendChild(img)
        a2.href = "detalledeproducto.html"
        a2.setAttribute("onclick", "guardar(this.parentNode.id)")
        //br1.textContent=`"${articulos[numero].nombreCompletoUsuario}"`
        //br2.textContent=`"${articulos[numero].nombreDeProducto}"`
        a2.innerHTML = `<br>${articulos[numero].nombreDeProducto}<br> Por ${articulos[numero].nombreCompletoUsuario}`
        var fecha=new Date(articulos[numero].fecha)
        var mes=fecha.getMonth()+1
        var minutos=fecha.getMinutes()
        if( minutos.toString().length==1){
             minutos="0"+fecha.getMinutes()
        }
        a3.innerHTML =`<br>${' '+fecha.getDate()+'/'+mes+'/'+fecha.getFullYear()+' a las '+fecha.getHours()+':'+minutos}`
        parentdiv.appendChild(a1)
        parentdiv.appendChild(a2)
        parentdiv.appendChild(a3)
        lista.appendChild(parentdiv)


    }

}

function guardar(id) {
    sessionStorage.setItem('DetalleArticulo', JSON.stringify(id))
}

function cerrarSesion(){
    sessionStorage.removeItem("UsuarioSesion");
    window.location.reload();
}

function obtenerUsuario() {
    var usuario = JSON.parse(sessionStorage.getItem('UsuarioSesion'));
    return usuario
}
